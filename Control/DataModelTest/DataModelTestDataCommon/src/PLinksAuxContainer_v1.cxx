/*
   Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
 */
/**
 * @file DataModelTestDataCommon/src/PLinksAuxContainer_v1.cxx
 * @author scott snyder <snyder@bnl.gov>
 * @date Nov, 2023
 * @brief For testing packed links.
 */


#include "DataModelTestDataCommon/versions/PLinksAuxContainer_v1.h"


namespace DMTest {


PLinksAuxContainer_v1::PLinksAuxContainer_v1()
  : xAOD::AuxContainerBase()
{
}


} // namespace DMTest
