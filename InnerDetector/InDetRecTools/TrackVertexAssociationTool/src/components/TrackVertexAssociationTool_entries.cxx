#include "TrackVertexAssociationTool/TrackVertexAssociationTool.h"
#include "TrackVertexAssociationTool/MVATrackVertexAssociationTool.h"

#include "../TrackVertexAssoTestAlg.h"
#include "../PV0TrackSelectionAlg.h"

using namespace xAOD;

DECLARE_COMPONENT( CP::TrackVertexAssociationTool )
DECLARE_COMPONENT( CP::MVATrackVertexAssociationTool )
DECLARE_COMPONENT( TrackVertexAssoTestAlg )
DECLARE_COMPONENT( PV0TrackSelectionAlg )

